<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Cajon extends Model
{
    protected $table = 'cajones';
    protected $fillable = [
        'tipo_de_cajon_id',
        'usuario_id',
    ];
    public $timestamps = false;
    
    public function tipoDeCajon(){
        return $this->belongsTo('App\Models\TipoDeCajon', 'tipo_de_cajon_id', 'id');
    }
    public function fases(){
        return $this->hasMany('App\Models\FaseDeCajon', 'cajon_id', 'id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }
    public function ultimoEstado(){
        $this->fases->last()->estado;
    }
    public function estadosSiguientes(){
        return $this->ultimoEstado()->estadosSiguientes();
    }
    /*
    Fases de cajon
        01 Creado---------------------> ::crearCajon(tipo,obs) o ::crearCajonConColmena(tipo,obs,especie)
        02 Colmena instalada----------> instalarColmena(colmena,obs)
        03 Colmena mudada-------------> mudarColmena(colmena,obs)
        04 Instalado en meliponario---> instalarEnMeliponario(meliponario, obs)
        05 Trasladado de meliponario--> mudarHaciaMeliponario(meliponario, obs)
        06 Revisado-------------------> revisar(obs)
        07 Pendiente de reparación----> pendienteReparacion(obs)
        08 Llevado a reparar----------> llevarAReparar(obs)
        09 Reparado-------------------> reparado(obs)
        10 Baja-----------------------> baja(obs) baja colmena
        11 Desarmado para almacenar---> desarmado(obs)
    */
    public static function crearCajon(TipoDeCajon $tipo = null, String $obs='', Carbon $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' Cajon::crearCajon(..)
                ');
            }
        }
        $cajon = null;
        $user = auth()->user();
        if($user == null){
            $user = User::random();
        }
        $cajon = new Cajon([
            'usuario_id'=>auth()->user()->id,
        ]);
        if($tipo != null){
            $cajon->tipo_de_cajon_id = $tipo->id;
            $cajon->save();
            $cajon->refresh();
        }
        $fase = new FaseDeCajon([
            'fecha'              => $fecha,
            'cajon_id'           => $cajon->id,
            'estado_de_cajon_id' => 1,
            'descripcion'        => $obs,
        ]);
        return $fase;
    }
    public static function crearCajonConColmena(TipoDeCajon $tipo = null, String $obs='', Especie $especie = null, Carbon $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' Cajon::crearCajonConColmena(..)
                ');
            }
        }
        $fase_cajon = Cajon::crearCajon($tipo,$obs,$fecha);
        $cajon = $fase_cajon->cajon;
        Colmena::crearColmenaConCajon($cajon,$especie,$obs,$fecha);
        return $fase_cajon;
    }
    public function instalarColmena(Colmena $colmena, String $obs='', Carbon $fecha = null){
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cajon->instalarColmena(..)
                ');
            }
        }
        if($colmena->ultimoEstado()->id == 2){
            if($this->estadosSiguientes()->pluck('id')->contains(2)){
                $fase = new FaseDeCajon([
                    'fecha'              => $fecha,
                    'cajon_id'           => $this->id,
                    'estado_de_cajon_id' => 2,
                    'descripcion'        => $obs,
                    'colmena_id'         => $colmena->id,
                ]);
                return $fase;
            }
            dd('Error 6548548521');
        }
        dd('Error 2+941');
    }
    public function mudarColmena(Colmena $colmena, String $obs='', Carbon $fecha = null){
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cajon->mudarColmena(..)
                ');
            }
        }
        if($colmena->ultimoEstado()->id == 3){
            if($this->estadosSiguientes()->pluck('id')->contains(3)){
                $fase = new FaseDeCajon([
                    'fecha'              => $fecha,
                    'cajon_id'           => $this->id,
                    'estado_de_cajon_id' => 3,
                    'descripcion'        => $obs,
                    'colmena_id'         => $colmena->id,
                ]);
                return $fase;
            }
            dd('Error 88452201');
        }
        dd('Error 95578/');
    }
}
