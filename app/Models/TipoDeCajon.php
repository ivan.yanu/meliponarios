<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDeCajon extends Model
{
    protected $table = 'tipos_de_cajones';
    protected $fillable = [
        'nombre',     
        'descripcion',
    ];
    public $timestamps = false;
    public function cajon(){
        return $this->hasMany('App\Models\Cajon', 'tipo_de_cajon_id', 'id');
    }
}
