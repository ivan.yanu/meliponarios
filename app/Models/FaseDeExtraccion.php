<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaseDeExtraccion extends Model
{
    protected $table = 'fases_de_extracciones';
    protected $fillable = [
        'fecha',
        'extraccion_id',
        'estado_de_extraccion_id',
        'descripcion',
        'extraccion_por_lote_id',//nullable
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    public function extraccion(){
        return $this->belongsTo('App\Models\Extraccion', 'extraccion_id', 'id');
    }
    public function estado(){
        return $this->belongsTo('App\Models\EstadoDeExtraccion', 'estado_de_extraccion_id', 'estado_de_extraccion_id');
    }
    public function extraccionPorLote(){
        return $this->belongsTo('App\Models\ExtraccionPorLote', 'extraccion_por_lote_id', 'id');
    }
}
