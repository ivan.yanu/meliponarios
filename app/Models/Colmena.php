<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

class Colmena extends Model
{
    protected $table = 'colmenas';
    protected $fillable = [
        'especie_id',
        'usuario_id',
    ];
    public $timestamps = false;
    
    public function especie(){
        return $this->belongsTo('App\Models\Especie', 'especie_id', 'id');
    }
    public function fases(){
        return $this->hasMany('App\Models\FaseDeColmena', 'colmena_id', 'id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }
    public function extracciones(){
        $extracciones = new Collection();
        $fases = $this->fases;
        foreach($fases as $fase){
            if($fase->extraccion() != null){
                $extracciones->add($fase->extraccion());
            }
        }
        return $extracciones;
    }
    public function ultimoEstado(){
        $this->fases->last()->estado;
    }
    public function estadosSiguientes(){
        return $this->ultimoEstado()->estadosSiguientes();
    }
    /*
    Fases de colmena:
        01 Creada-----------------> ::crearColmena(especie,obs)
        02 Instalada en un cajón--> intalarEnCajon(cajon,obs) o ::crearColmenaConCajon(cajon,especie,obs)
        03 Mudada de cajón--------> quitarDeCajon(cajon,obs)
        04 Separada---------------> separarHaciaUnCajon(cajon,obs)
        05 Melada-----------------> melar(peso_en_gramos,obs)
        06 Tratamiento------------> tratamiento(obs)
        07 Baja-------------------> baja(obs)
        08 Revisada por fuera-----> revisarPorFuera(obs)
        09 Revisada por dentro----> revisarPorDentro(obs)
    */
    public static function crearColmena(Especie $especie = null, String $obs='', Carbon  $fecha = null){
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' Colmena::crearColmena(..)
                ');
            }
        }
        $colmena = null;
        $user = auth()->user();
        if($user == null){
            $user = User::random();
        }
        $colmena = new Colmena([
            'usuario_id'=>auth()->user()->id,
        ]);
        if($especie != null){
            $colmena->especie_id = $especie->id;
            $colmena->save();
            $colmena->refresh();
        }
        $fase = new FaseDeColmena([
            'fecha'                => $fecha,
            'colmena_id'           => $colmena->id,
            'estado_de_colmena_id' => 1,
            'descripcion'          => $obs,
        ]);
        return $fase;
    }
    public static function crearColmenaConCajon(Cajon $cajon, Especie $especie = null, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' Colmena::crearColmenaConCajon(..)
                ');
            }
        }
        $fase_colmena = Colmena::crearColmena($especie,$obs,$fecha);
        $fase_colmena = $fase_colmena->colmena->instalarEnCajon($cajon,$obs,$fecha);
        if($fase_colmena != null){
            return $fase_colmena;
        }
        dd('Error 99558851');
    }
    public function intalarEnCajon(Cajon $cajon, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->intalarEnCajon(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(2)){
            $fase = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 2,
                'descripcion'          => $obs,
                'cajon_id'             => $cajon->id,
            ]);
            if($cajon->instalarColmena($this,$obs,$fecha) == null){
                dd('Error 958522');
            }
            return $fase;
        }
        dd('Error 285217');
    }
    public function quitarDeCajon(Cajon $cajon, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->quitarDeCajon(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(3)){
            $fase = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 3,
                'descripcion'          => $obs,
                'cajon_id'             => $cajon->id,
            ]);
            if($cajon->mudarColmena($this,$obs,$fecha) == null){
                dd('Error 02522125');
            }
            return $fase;
        }
        dd('Error 395128525');
    }
    public function separarHaciaUnCajon(Cajon $cajon, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->qsepararHaciaUnCajon(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(4)){
            $fase = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 4,
                'descripcion'          => $obs,
                'cajon_id'             => $cajon->id,
            ]);
            if(Colmena::crearColmenaConCajon($cajon,$this->especie,$obs,$fecha) == null){
                dd('Error ]85568455');
            }
            return $fase;
        }
        dd('Error edfrfds');
    }
    public function melar(int $peso_en_gramos=0, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->melar(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(5)){
            $fase_colmena = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 5,
                'descripcion'          => $obs,
            ]);
            $fase_extraccion = Extraccion::crear($this,$peso_en_gramos,$obs,$fecha);
            $fase_colmena->extraccion_id = $fase_extraccion->extraccion->id;
            $fase_colmena->save();
            $fase_colmena->refresh();
            return $fase_colmena;
        }
        dd('Error 4053.015');
    }
    public function tratamiento(String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->tratamiento(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(6)){
            $fase = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 6,
                'descripcion'          => $obs,
            ]);
            return $fase;
        }
        dd('Error 80023156');
    }
    public function baja(String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->baja(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(7)){
            $fase = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 7,
                'descripcion'          => $obs,
            ]);
            return $fase;
        }
        dd('Error 81255666');
    }
    public function revisarPorFuera(String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->revisarPorFuera(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(8)){
            $fase = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 8,
                'descripcion'          => $obs,
            ]);
            return $fase;
        }
        dd('Error 851114788');
    }
    public function revisarPorDentro(String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $colmena->revisarPorDentro(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(9)){
            $fase = new FaseDeColmena([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 9,
                'descripcion'          => $obs,
            ]);
            return $fase;
        }
        dd('Error 012561586');
    }
}
