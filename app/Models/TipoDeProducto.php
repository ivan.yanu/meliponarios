<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoDeProducto extends Model
{
    protected $table = 'tipos_de_productos';
    protected $fillable = [
        'nombre',     
        'descripcion',
        'peso_en_gramos',
    ];
    public $timestamps = false;
    
    public function productos(){
        return $this->hasMany('App\Models\Producto', 'tipo_de_producto_id', 'id');
    }
    
}
