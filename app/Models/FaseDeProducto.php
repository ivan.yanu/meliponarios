<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaseDeProducto extends Model
{
    protected $table = 'fases_de_productos';
    protected $fillable = [
        'fecha',
        'producto_id',
        'estado_de_producto_id',
        'descripcion',
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    public function producto(){
        return $this->belongsTo('App\Models\Producto', 'producto_id', 'id');
    }
    public function estado(){
        return $this->belongsTo('App\Models\EstadoDeProducto', 'estado_de_producto_id', 'estado_de_producto_id');
    }
}
