<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Models;

class TipoDeCebo extends Model
{
    protected $table = 'tipos_de_cebos';
    protected $fillable = [
        'nombre',     
        'descripcion',
    ];
    public $timestamps = false;
    public function cebos(){
        return $this->hasMany('App\Models\Cebo','tipo_de_cebo_id','id');
    }
}
