<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaseDeCajon extends Model
{
    protected $table = 'fases_de_cajones';
    protected $fillable = [
        'fecha',
        'cajon_id',
        'estado_de_cajon_id',
        'descripcion',
        'colmena_id',
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    
    public function cajon(){
        return $this->belongsTo('App\Models\Cajon', 'cajon_id', 'id');
    }
    public function estado(){
        return $this->belongsTo('App\Models\EstadoDeCajon', 'estado_de_cajon_id', 'estado_de_cajon_id');
    }
    public function colmena(){
        return $this->belongsTo('App\Models\Colmena', 'colmena_id', 'id');
    }
    
}
