<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtraccionPorLote extends Model
{
    protected $table = 'extracciones_por_lotes';
    protected $fillable = [
        'extraccion_id',
        'lote_id',
        'peso_en_gramos',
        'pasteurizado',
        'fecha',
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    
    public function extraccion(){
        return $this->belongsTo('App\Models\Extraccion', 'extraccion_id', 'id');
    }
    public function lote(){
        return $this->belongsTo('App\Models\Lote', 'lote_id', 'id');
    }
    
}
