<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
    protected $fillable = [
        'lote_id',
        'tipo_de_producto_id',
    ];
    public $timestamps = false;
    public function fases(){
        return $this->hasMany('App\Models\FaseDeProducto', 'producto_id', 'id');
    }
    public function tipoDeProducto(){
        return $this->belongsTo('App\Models\TipoDeProducto', 'tipo_de_producto_id', 'id');
    }
    
}
