<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meliponario extends Model
{
    protected $table = 'meliponarios';
    protected $fillable = [
        'nombre',     
        'descripcion',
        'usuario_id',
    ];
    public $timestamps = false;
    public function fases(){
        return $this->hasMany('App\Models\FaseDeMeliponario', 'meliponario_id', 'id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }
}
