<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaseDeColmena extends Model
{
    protected $table = 'fases_de_colmenas';
    protected $fillable = [
        'fecha',
        'colmena_id',
        'estado_de_colmena_id',
        'descripcion',
        'cajon_id',//nullable
        'extraccion_id',//nullable
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    public function colmena(){
        return $this->belongsTo('App\Models\Colmena', 'colmena_id', 'id');
    }
    public function estado(){
        return $this->belongsTo('App\Models\EstadoDeColmena', 'estado_de_colmena_id', 'estado_de_colmena_id');
    }
    public function extraccion(){
        return $this->belongsTo('App\Models\Extraccion', 'extraccion_id', 'id');
    }
    public function extracciones(){
        return $this->hasMany('App\Models\Extraccion', 'fase_de_colmena_id', 'id');
    }
    public function cajon(){
        return $this->belongsTo('App\Models\Cajon', 'cajon_id', 'id');
    }
}
