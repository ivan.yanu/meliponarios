<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdenEstadosDeColmena extends Model
{
    protected $table = 'orden_estados_de_colmenas';
    protected $fillable = [
        'estado_inicial_id',
        'estado_siguiente_id',
    ];
    public $timestamps = false;
    public function estadoInicial(){
        return $this->belongsTo('App\Models\EstadoDeColmena', 'estado_inicial_id', 'id');
    }
    public function estadoSiguiente(){
        return $this->belongsTo('App\Models\EstadoDeColmena', 'estado_siguiente_id', 'id');
    }
}
