<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cebo extends Model
{
    protected $table = 'cebos';
    protected $fillable = [
        'tipo_de_cebo_id',
        'usuario_id',
    ];
    public $timestamps = false;

    public function tipoDeCebo(){
        return $this->belongsTo('App\Models\TipoDeCebo','tipo_de_cebo_id','id');
    }
    public function fases(){
        return $this->hasMany('App\Models\FaseDeCebo', 'cebo_id', 'id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }
    public function ultimoEstado(){
        $this->fases->last()->estado;
    }
    public function estadosSiguientes(){
        return $this->ultimoEstado()->estadosSiguientes();
    }
    public function colmena(){
        $colmena = null;
        $fases = $this->fases;
        foreach($fases as $fase){
            if($fase->colmena != null){
                $colmena = $fase->colmena;
            }
        }
        return $colmena;
    }
    /*
    Fases:
        01 Creado--------------------> ::crear(tipo_de_cebo,obs)
        02 Instalado en meliponario--> instalarEnMeliponario(meliponario, obs)
        03 Mudado de meliponario-----> moverAMeliponario(meliponario, obs)
        04 Abejas encontradas--------> colmenaCapturada(especie,obs)
        05 Baja----------------------> baja(obs)
        06 Pasado a cajon------------> pasarACajon(cajon, obs)
        07 Revisado------------------> revisar(obs)
    */
    public static function crear(TipoDeCebo $tipo_de_cebo = null, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' Cebo::crear(..)
                ');
            }
        }
        $user = auth()->user();
        if($user == null){
            $user = User::random();
        }
        $cebo = new Cebo([
            'usuario_id'     => $user->id,
        ]);
        if($tipo_de_cebo != null){
            $cebo->tipo_de_cebo_id = $tipo_de_cebo->id;
            $cebo->save();
            $cebo->refresh();
        }
        $fase = new FaseDeCebo([
            'fecha'             => $fecha,
            'cebo_id'           => $cebo->id,
            'estado_de_cebo_id' => 1,
            'descripcion'       => $obs,
        ]);
        return $fase;
    }
    public function instalarEnMeliponario(Meliponario $meliponario, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cebo->instalarEnMeliponario(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(2)){
            $fase = new FaseDeCebo([
                'fecha'             => $fecha,
                'cebo_id'           => $this->id,
                'estado_de_cebo_id' => 2,
                'descripcion'       => $obs,
                'meliponario_id'    => $meliponario->id,
            ]);
            return $fase;
        }
        dd('Error 3262555');
    }
    public function moverAMeliponario(Meliponario $meliponario, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cebo->moverAMeliponario(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(3)){
            $fase = new FaseDeCebo([
                'fecha'             => $fecha,
                'cebo_id'           => $this->id,
                'estado_de_cebo_id' => 3,
                'descripcion'       => $obs,
                'meliponario_id'    => $meliponario->id,
            ]);
            return $fase;
        }
        dd('Error 41125488');
    }
    public function colmenaCapturada(Especie $especie = null, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cebo->colmenaCapturada(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(4)){
            $fase_colmena = Colmena::crearColmena($especie,$obs,$fecha);
            $fase = new FaseDeCebo([
                'fecha'             => $fecha,
                'cebo_id'           => $this->id,
                'estado_de_cebo_id' => 4,
                'descripcion'       => $obs,
                'colmena_id'        => $fase_colmena->colmena->id,
            ]);
            return $fase;
        }
        dd('Error 9548558');
    }
    public function baja(String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cebo->baja(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(5)){
            if($this->colmena() != null){
                $this->colmena()->baja();
            }
            $fase = new FaseDeCebo([
                'fecha'             => $fecha,
                'cebo_id'           => $this->id,
                'estado_de_cebo_id' => 5,
                'descripcion'       => $obs,
            ]);
            return $fase;
        }
        dd('Error 77841221');
    }
    public function pasarACajon(Cajon $cajon, String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cebo->pasarACajon(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(6)){
            if($this->colmena() != null){
                $this->colmena()->instalarEnCajon($cajon,$obs,$fecha);
                $fase = new FaseDeCebo([
                    'fecha'             => $fecha,
                    'cebo_id'           => $this->id,
                    'estado_de_cebo_id' => 6,
                    'descripcion'       => $obs,
                    'cajon_id'          => $cajon->id,
                ]);
                return $fase;
            }
            dd('Error 1114522666');
        }
        dd('Error 42633666');
    }
    public function revisar(String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $cebo->revisar(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(7)){
            $fase = new FaseDeCebo([
                'fecha'             => $fecha,
                'cebo_id'           => $this->id,
                'estado_de_cebo_id' => 7,
                'descripcion'       => $obs,
            ]);
            return $fase;
        }
        dd('Error 13055895');
    }
}
