<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaseDeMeliponario extends Model
{
    protected $table = 'fases_de_meliponarios';
    protected $fillable = [
        'fecha',
        'meliponario_id',
        'estado_de_meliponario_id',
        'descripcion',
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    public function meliponario(){
        return $this->belongsTo('App\Models\Meliponario', 'meliponario_id', 'id');
    }
    public function estado(){
        return $this->belongsTo('App\Models\EstadoDeMeliponario', 'estado_de_meliponario_id', 'estado_de_meliponario_id');
    }
}
