<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Especie extends Model
{
    protected $table = 'especies';
    protected $fillable = [
        'nombre_cientifico',
        'nombre_comun',
        'descripcion',
    ];
    public $timestamps = false;
    
    public function colmenas(){
        return $this->hasMany('App\Models\Colmena', 'especie_id', 'id');
    }
    
}
