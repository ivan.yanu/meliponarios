<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaseDeCebo extends Model
{
    protected $table = 'fases_de_cebos';
    protected $fillable = [
        'fecha',
        'cebo_id',
        'estado_de_cebo_id',
        'descripcion',
        'meliponario_id',//nullable
        'colmena_id',//nullable
        'cajon_id',//nullable
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    public function cebo(){
        return $this->belongsTo('App\Models\Cebo', 'cebo_id', 'id');
    }
    public function estado(){
        return $this->belongsTo('App\Models\EstadoDeCebo', 'estado_de_cebo_id', 'estado_de_cebo_id');
    }
    public function meliponario(){
        return $this->belongsTo('App\Models\Meliponario', 'meliponario_id', 'id');
    }
    public function colmena(){
        return $this->belongsTo('App\Models\Colmena', 'colmena_id', 'id');
    }
    public function cajon(){
        return $this->belongsTo('App\Models\Cajon', 'cajon_id', 'id');
    }
}
