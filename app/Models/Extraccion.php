<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Extraccion extends Model
{
    protected $table = 'extracciones';
    protected $fillable = [
        'peso_en_gramos',
        'colmena_id',
        'usuario_id',
    ];
    public $timestamps = false;
    
    public function extraccionesPorLotes(){
        return $this->belongsTo('App\Models\ExtraccionPorLote', 'extraccion_id', 'id');
    }
    public function fases(){
        return $this->hasMany('App\Models\FaseDeExtraccion', 'extraccion_id', 'id');
    }
    public function lotes(){
        $lotes = new Collection();
        $extracciones_por_lotes = $this->extraccionesPorLotes;
        foreach($extracciones_por_lotes as $extraccion_por_lote){
            $lotes = $extraccion_por_lote->lote;
        }
        return $lotes;
    }
    public function colmena(){
        return $this->belongsTo('App\Models\Colmena', 'colmena_id', 'id');
    }
    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id', 'id');
    }
    public function ultimoEstado(){
        $this->fases->last()->estado;
    }
    public function estadosSiguientes(){
        return $this->ultimoEstado()->estadosSiguientes();
    }
    public function estaPasteurizada(){
        $pasteurizado = false;
        $fases = $this->fases;
        foreach($fases as $fase){
            if($fase->estado_de_colmena_id ==2){
                $pasteurizado = true;
            }
        }
        return $pasteurizado;
    }
    public function pesoFraccionado(){
        $peso = 0;
        $fases = $this->fases;
        foreach($fases as $fase){
            if($fase->estado_de_extraccion_id == 3){
                
            }
        }
    }
    /*
    Fases:
        01 Realizada-----------> ::crear(fase_de_colmena, peso, obs)
        02 Pasteurizada--------> pasteurizar(obs)
        03 Fraccionada---------> fraccionar(lote,finalizar_automaticamente, perso, obs)
        04 Finalizada---------->
        05 Control de calidad-->
        06 Baja---------------->
    */
    public static function crear(Colmena $colmena = null, int $peso_en_gramos = 0 ,String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' Extraccion::crear(..)
                ');
            }
        }
        $user = auth()->user();
        if($user == null){
            $user = User::random();
        }
        $extraccion = new Extraccion([
            'peso_en_gramos' => $peso_en_gramos,
            'usuario_id'     => $user->id,
        ]);
        if($colmena != null){
            $extraccion->colmena_id = $colmena->id;
            $extraccion->save();
            $extraccion->refresh();
        }
        $fase = new FaseDeExtraccion([
            'fecha'                   => $fecha,
            'extraccion_id'           => $extraccion->id,
            'estado_de_extraccion_id' => 1,
            'descripcion'             => $obs,
        ]);
        return $fase;
    }
    public function pasteurizar(String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $extraccion->pasteurizar(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(2)){
            $fase = new FaseDeExtraccion([
                'fecha'                => $fecha,
                'colmena_id'           => $this->id,
                'estado_de_colmena_id' => 2,
                'descripcion'          => $obs,
            ]);
            return $fase;
        }
        dd('Error 200445248');
    }
    public function fraccionar(Lote $lote, int $finalizar_automaticamente = 1, int $peso_en_gramos = 0,String $obs='', Carbon  $fecha = null){
        //FRONT
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $extraccion->fraccionar(..)
                ');
            }
        }
        $extraccion_por_lote = new ExtraccionPorLote([
            'extraccion_id'  => $this->id,
            'lote_id'        => $lote->id,
            'peso_en_gramos' => $peso_en_gramos,
            'pasteurizado'   => $this->estaPasteurizada(),
            'fecha'          => $fecha,
        ]);
        if($this->estadosSiguientes()->pluck('id')->contains(3)){
            $fase = new FaseDeExtraccion([
                'fecha'                   => $fecha,
                'colmena_id'              => $this->id,
                'estado_de_extraccion_id' => 3,
                'descripcion'             => $obs,
                'extraccion_por_lote_id'  => $extraccion_por_lote->id,
            ]);
            $lote->cargarExtraccion($extraccion_por_lote,$obs,$fecha);
            if(($finalizar_automaticamente) && ($this->peso_en_gramos != 0)){
                
            }
            return $fase;
        }

        dd('Error 200445248');
    }

}
