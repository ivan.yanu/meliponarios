<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrdenEstadosDeProducto extends Model
{
    protected $table = 'orden_estados_de_productos';
    protected $fillable = [
        'estado_inicial_id',
        'estado_siguiente_id',
    ];
    public $timestamps = false;
    public function estadoInicial(){
        return $this->belongsTo('App\Models\EstadoDeProducto', 'estado_inicial_id', 'id');
    }
    public function estadoSiguiente(){
        return $this->belongsTo('App\Models\EstadoDeProducto', 'estado_siguiente_id', 'id');
    }
}
