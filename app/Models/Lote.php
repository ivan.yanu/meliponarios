<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lote extends Model
{
    protected $table = 'lotes';
    protected $fillable = [
        
    ];
    public $timestamps = false;
    public function extraccionesPorLotes(){
        return $this->belongsTo('App\Models\ExtraccionPorLote', 'lote_id', 'id');
    }
    public function fases(){
        return $this->hasMany('App\Models\FaseDeLote', 'lote_id', 'id');
    }
    public function extracciones(){
        $extracciones = new Collection();
        $extracciones_por_lotes = $this->extraccionesPorLotes;
        foreach($extracciones_por_lotes as $extraccion_por_lote){
            $extracciones = $extraccion_por_lote->extraccion;
        }
        return $extracciones;
    }
    /*
    Fases:
        01 Creado---------------> ::crear(obs) o ::crearConextraccion(extraccion, peso, observaciones)
        02 Carga de extracción--> cargarExtraccion(extraccion_por_lote,obs)
        03 Completo-------------> 
        04 Cerrado--------------> 
        05 Pasteurizado---------> 
        06 Pesado---------------> 
        07 Abierto--------------> 
        08 Fraccionado----------> 
        09 Finalizado-----------> 
        10 Control de calidad---> 
        11 Baja-----------------> 
    */
    public function cargarExtraccion(ExtraccionPorLote $extraccion_por_lote,String $obs='', Carbon  $fecha = null){
        if($fecha == null){
            $fecha = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($fecha) > 180) {
                echo($fecha.' $lote->cargarExtraccion(..)
                ');
            }
        }
        if($this->estadosSiguientes()->pluck('id')->contains(2)){
            $fase = new FaseDeExtraccion([
                'fecha'                  => $fecha,
                'colmena_id'             => $this->id,
                'estado_de_colmena_id'   => 2,
                'descripcion'            => $obs,
                'extraccion_por_lote_id' => $extraccion_por_lote->id,
            ]);
            return $fase;
        }
        echo('Error 201205852');
    }
}
