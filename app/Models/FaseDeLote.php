<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaseDeLote extends Model
{
    protected $table = 'fases_de_lotes';
    protected $fillable = [
        'fecha',
        'lote_id',
        'estado_de_lote_id',
        'descripcion',
        'extraccion_por_lote_id',//nullable
    ];
    public $timestamps = false;
    protected $dates = [
        'fecha',
    ];
    public function lote(){
        return $this->belongsTo('App\Models\Lote', 'lote_id', 'id');
    }
    public function estado(){
        return $this->belongsTo('App\Models\EstadoDeLote', 'estado_de_lote_id', 'estado_de_lote_id');
    }
    public function extraccionPorLote(){
        return $this->belongsTo('App\Models\ExtraccionPorLote', 'extraccion_por_lote_id', 'id');
    }
    
}
