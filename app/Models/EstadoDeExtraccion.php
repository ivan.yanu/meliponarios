<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoDeExtraccion extends Model
{
    protected $table = 'estados_de_extracciones';
    protected $fillable = [
        'nombre',     
        'descripcion',
    ];
    public $timestamps = false;
    public function ordenesEstados(){
        return $this->hasMany('App\Models\OrdenEstadosDeExtraccion','estado_inicial_id','id');
    }
    public function estadosSiguientes(){
        $ordenes_estados = $this->ordenesEstados;
        $siguientes = new Collection();
        foreach ($ordenes_estados as $orden_estado){
            $siguientes->add($orden_estado->estadoSiguiente);
        }
        return $siguientes;
    }
    public function fases(){
        return $this->hasMany('App\Models\FaseDeExtraccion', 'estado_de_extraccion_id', 'id');
    }
}
