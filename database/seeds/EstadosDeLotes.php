<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoDeLote;

class EstadosDeLotes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Carga de extracción
            03 Completo
            04 Cerrado
            05 Pasteurizado
            06 Pesado
            07 Abierto
            08 Fraccionado
            09 Finalizado
            10 Control de calidad
            11 Baja
        */
        $nombres = [
            'Creado',
            'Carga de extracción',
            'Completo',
            'Cerrado',
            'Pasteurizado',
            'Pesado',
            'Abierto',
            'Fraccionado',
            'Finalizado ',
            'Control de calidad',
            'Baja',
        ];
        $descripciones = [
            'Lote creado',
            'Se agregó una extracción al lote',
            'El lote se completó',
            'El lote fue cerrado',
            'El lote pasó por unS proceso de pasteurización',
            'Se registró el peso del lote',
            'El lote fue abierto',
            'Se fraccionó un producto del lote',
            'El lote se fraccionó completamente',
            'Se realizó un examen de control de calidad aleatorio sobre el lote',
            'El lote fué dado de baja',
        ];
        for ($i=0 ; $i<11 ; $i++){
            EstadoDeLote::create([
                'nombre'        =>$nombres[$i],
                'descripcion'   =>$descripciones[$i],
            ]);
        }
    }
}
