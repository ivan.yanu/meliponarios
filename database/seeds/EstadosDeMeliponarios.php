<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoDeMeliponario;

class EstadosDeMeliponarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Baja
        */
        $nombres = [
            'Creado',
            'Baja',
        ];
        $descripciones = [
            'El meliponario fue dado de alta',
            'El meliponario fue dado de baja',
        ];
        for ($i=0 ; $i<2 ; $i++){
            EstadoDeMeliponario::create([
                'nombre'        =>$nombres[$i],
                'descripcion'   =>$descripciones[$i],
            ]);
        }
    }
}
