<?php

use Illuminate\Database\Seeder;
use App\Models\OrdenEstadosDeProducto;

class OrdenEstadosDeProductos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Pasteurizado
            03 Reservado
            04 Vendido
            05 Control de calidad
            06 Baja
            07 Almacenado
        */
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 2,//Pasteurizado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 3,//Reservado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 4,//Vendido
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 5,//Control de calidad
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 6,//Baja
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 7,//Almacenado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 2,//Pasteurizado
            'estado_siguiente_id' => 3,//Reservado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 2,//Pasteurizado
            'estado_siguiente_id' => 4,//Vendido
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 2,//Pasteurizado
            'estado_siguiente_id' => 5,//Control de calidad
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 2,//Pasteurizado
            'estado_siguiente_id' => 6,//Baja
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 2,//Pasteurizado
            'estado_siguiente_id' => 7,//Almacenado
        ]);
        
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 3,//Reservado
            'estado_siguiente_id' => 2,//Pasteurizado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 3,//Reservado
            'estado_siguiente_id' => 4,//Vendido
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 3,//Reservado
            'estado_siguiente_id' => 5,//Control de calidad
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 3,//Reservado
            'estado_siguiente_id' => 6,//Baja
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 3,//Reservado
            'estado_siguiente_id' => 7,//Almacenado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 5,//Control de calidad
            'estado_siguiente_id' => 2,//Pasteurizado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 5,//Control de calidad
            'estado_siguiente_id' => 3,//Reservado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 5,//Control de calidad
            'estado_siguiente_id' => 4,//Vendido
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 5,//Control de calidad
            'estado_siguiente_id' => 6,//Baja
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 5,//Control de calidad
            'estado_siguiente_id' => 7,//Almacenado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 7,//Almacenado
            'estado_siguiente_id' => 2,//Pasteurizado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 7,//Almacenado
            'estado_siguiente_id' => 3,//Reservado
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 7,//Almacenado
            'estado_siguiente_id' => 4,//Vendido
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 7,//Almacenado
            'estado_siguiente_id' => 5,//Control de calidad
        ]);
        OrdenEstadosDeProducto::create([
            'estado_inicial_id'   => 7,//Almacenado
            'estado_siguiente_id' => 6,//Baja
        ]);
    }
}
