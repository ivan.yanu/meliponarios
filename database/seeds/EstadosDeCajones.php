<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoDeCajon;

class EstadosDeCajones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Colmena instalada
            03 Colmena mudada
            04 Instalado en meliponario
            05 Trasladado de meliponario
            06 Revisado
            07 Pendiente de reparación
            08 Llevado a reparar
            09 Reparado
            10 Baja
            11 Desarmado para almacenar
        */
        $nombres = [
            'Creado',
            'Colmena instalada',
            'Colmena mudada',
            'Instalado en meliponario',
            'Trasladado de meliponario',
            'Revisado',
            'Pendiente de reparación',
            'Llevado a reparar',
            'Reparado',
            'Baja',
            'Desarmado para almacenar',
        ];
        $descripciones = [
            'El cajon fue creado',
            'Se instaló una nueva colmena al cajón',
            'Se quitó la colmena que tenía el cajón',
            'El cajón fue instalado en un meliponario',
            'El cajón fué mudado de meliponario',
            'El cajón fué revisado',
            'El cajon se marcó como pendiente de reparación',
            'Se llevó el cajon para ser reparado',
            'El cajón se reparó exitosamente',
            'El cajón se perdió o destruyó',
            'El cajon fue desarmado y se guardó'
        ];
        for ($i=0 ; $i<11 ; $i++){
            EstadoDeCajon::create([
                'nombre'        =>$nombres[$i],
                'descripcion'   =>$descripciones[$i],
            ]);
        }
    }
}
