<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoDeCebo;

class EstadosDeCebos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Instalado en meliponario
            03 Mudado de meliponario
            04 Abejas encontradas
            05 Baja
            06 Pasado a cajon
            07 Revisado
        */
        $nombres = [
            'Creado',
            'Instalado en meliponario',
            'Mudado de meliponario',
            'Abejas encontradas',
            'Baja',
            'Pasado a cajon',
            'Revisado',
        ];
        $descripciones = [
            'El cebo fue construido',
            'El cebo fue instalado en un meliponario',
            'El cebo fue reinstalado en otro diferente',
            'El cebo capturó un encambre',
            'El cebo se perdió o destruyó',
            'El cebo se desarmó para pasar a un cajon',
            'El cebo fue inspeccionado en busca de abejas o anormalidades',          
        ];
        for ($i=0 ; $i<7 ; $i++){
            EstadoDeCebo::create([
                'nombre'        =>$nombres[$i],
                'descripcion'   =>$descripciones[$i],
            ]);
        }
    }
}
