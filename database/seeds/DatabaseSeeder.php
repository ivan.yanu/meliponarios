<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(EstadosDeCajones::class);
        $this->call(EstadosDeCebos::class);
        $this->call(EstadosDeColmenas::class);
        $this->call(EstadosDeExtracciones::class);
        $this->call(EstadosDeLotes::class);
        $this->call(EstadosDeMeliponarios::class);
        $this->call(EstadosDeProductos::class);
        $this->call(OrdenEstadosDeCajones::class);
        $this->call(OrdenEstadosDeCebos::class);
        $this->call(OrdenEstadosDeColmenas::class);
        $this->call(OrdenEstadosDeExtracciones::class);
        $this->call(OrdenEstadosDeLotes::class);
        $this->call(OrdenEstadosDeMeliponarios::class);
        $this->call(OrdenEstadosDeProductos::class);

    }
}
