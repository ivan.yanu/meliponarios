<?php

use Illuminate\Database\Seeder;
use App\Models\OrdenEstadosDeMeliponario;

class OrdenEstadosDeMeliponarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Baja
        */
        OrdenEstadosDeMeliponario::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 2,//Baja
        ]);
        OrdenEstadosDeMeliponario::create([
            'estado_inicial_id'   => 2,//Baja
            'estado_siguiente_id' => 1,//Creado
        ]);
    }
}
