<?php

use Illuminate\Database\Seeder;
use App\Models\OrdenEstadosDeColmena;

class OrdenEstadosDeColmenas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creada
            02 Instalada en un cajón
            03 Mudada de cajón
            04 Separada
            05 Melada
            06 Tratamiento
            07 Baja
            08 Revisada por fuera
            09 Revisada por dentro
        */
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 1,//Creada
            'estado_siguiente_id' => 2,//Instalada en un cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 1,//Creada
            'estado_siguiente_id' => 5,//Melada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 1,//Creada
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 2,//Instalada en un cajón
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 2,//Instalada en un cajón
            'estado_siguiente_id' => 4,//Separada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 2,//Instalada en un cajón
            'estado_siguiente_id' => 5,//Melada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 2,//Instalada en un cajón
            'estado_siguiente_id' => 6,//Tratamiento
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 2,//Instalada en un cajón
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 2,//Instalada en un cajón
            'estado_siguiente_id' => 8,//Revisada por fuera
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 2,//Instalada en un cajón
            'estado_siguiente_id' => 9,//Revisada por dentro
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 3,//Mudada de cajón
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 3,//Mudada de cajón
            'estado_siguiente_id' => 4,//Separada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 3,//Mudada de cajón
            'estado_siguiente_id' => 5,//Melada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 3,//Mudada de cajón
            'estado_siguiente_id' => 6,//Tratamiento
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 3,//Mudada de cajón
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 3,//Mudada de cajón
            'estado_siguiente_id' => 8,//Revisada por fuera
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 3,//Mudada de cajón
            'estado_siguiente_id' => 9,//Revisada por dentro
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 4,//Separada
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 4,//Separada
            'estado_siguiente_id' => 6,//Tratamiento
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 4,//Separada
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 4,//Separada
            'estado_siguiente_id' => 8,//Revisada por fuera
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 4,//Separada
            'estado_siguiente_id' => 9,//Revisada por dentro
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 2,//Instalada en un cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 4,//Separada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 5,//Melada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 6,//Tratamiento
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 8,//Revisada por fuera
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 5,//Melada
            'estado_siguiente_id' => 9,//Revisada por dentro
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 6,//Tratamiento
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 6,//Tratamiento
            'estado_siguiente_id' => 4,//Separada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 6,//Tratamiento
            'estado_siguiente_id' => 5,//Melada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 6,//Tratamiento
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 6,//Tratamiento
            'estado_siguiente_id' => 8,//Revisada por fuera
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 6,//Tratamiento
            'estado_siguiente_id' => 9,//Revisada por dentro
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 7,//Baja
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 8,//Revisada por fuera
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 8,//Revisada por fuera
            'estado_siguiente_id' => 4,//Separada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 8,//Revisada por fuera
            'estado_siguiente_id' => 5,//Melada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 8,//Revisada por fuera
            'estado_siguiente_id' => 6,//Tratamiento
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 8,//Revisada por fuera
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 8,//Revisada por fuera
            'estado_siguiente_id' => 8,//Revisada por fuera
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 8,//Revisada por fuera
            'estado_siguiente_id' => 9,//Revisada por dentro
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 9,//Revisada por dentro
            'estado_siguiente_id' => 3,//Mudada de cajón
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 9,//Revisada por dentro
            'estado_siguiente_id' => 4,//Separada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 9,//Revisada por dentro
            'estado_siguiente_id' => 5,//Melada
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 9,//Revisada por dentro
            'estado_siguiente_id' => 6,//Tratamiento
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 9,//Revisada por dentro
            'estado_siguiente_id' => 7,//Baja
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 9,//Revisada por dentro
            'estado_siguiente_id' => 8,//Revisada por fuera
        ]);
        OrdenEstadosDeColmena::create([
            'estado_inicial_id'   => 9,//Revisada por dentro
            'estado_siguiente_id' => 9,//Revisada por dentro
        ]);
    }
}
