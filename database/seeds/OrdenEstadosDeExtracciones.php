<?php

use Illuminate\Database\Seeder;
use App\Models\OrdenEstadosDeExtraccion;

class OrdenEstadosDeExtracciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Realizada
            02 Pasteurizada
            03 Fraccionada
            04 Finalizada
            05 Control de calidad
            06 Baja
        */
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 1,
            'estado_siguiente_id' => 2,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 1,
            'estado_siguiente_id' => 3,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 1,
            'estado_siguiente_id' => 5,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 1,
            'estado_siguiente_id' => 6,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 2,
            'estado_siguiente_id' => 3,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 2,
            'estado_siguiente_id' => 5,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 2,
            'estado_siguiente_id' => 6,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 3,
            'estado_siguiente_id' => 2,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 3,
            'estado_siguiente_id' => 3,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 3,
            'estado_siguiente_id' => 4,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 3,
            'estado_siguiente_id' => 5,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 3,
            'estado_siguiente_id' => 6,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 5,
            'estado_siguiente_id' => 2,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 5,
            'estado_siguiente_id' => 3,
        ]);
        OrdenEstadosDeExtraccion::create([
            'estado_inicial_id'   => 5,
            'estado_siguiente_id' => 6,
        ]);
    }
}
