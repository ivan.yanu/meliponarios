<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoDeColmena;

class EstadosDeColmenas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creada
            02 Instalada en un cajón
            03 Mudada de cajón
            04 Separada
            05 Melada
            06 Tratamiento
            07 Baja
            08 Revisada por fuera
            09 Revisada por dentro
        */
        $nombres = [
            'Creada',
            'Instalada en un cajón',
            'Mudada de cajón',
            'Separada',
            'Melada',
            'Tratamiento',
            'Baja',
            'Revisada por fuera',
            'Revisada por dentro',
        ];
        $descripciones = [
            'La colmena fue creada',
            'La colmena fue instalada en un cajón',
            'La colmena fue mudada de cajón',
            'Se realizó una separación de la colmena en otras',
            'Se extrajo miel de la colmena',
            'La colmena recibió ayuda para fortalecerce',
            'La colmena se perdió, destruyó o murió',
            'La colmena fue revisada por fuera en busqueda de anormalidades',
            'La colmena fue revisada por dentro en busqueda de anormalidades' 
        ];
        for ($i=0 ; $i<9 ; $i++){
            EstadoDeColmena::create([
                'nombre'        =>$nombres[$i],
                'descripcion'   =>$descripciones[$i],
            ]);
        }
    }
}
