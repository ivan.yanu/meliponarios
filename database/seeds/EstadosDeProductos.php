<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoDeProducto;

class EstadosDeProductos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Pasteurizado
            03 Reservado
            04 Vendido
            05 Control de calidad
            06 Baja
            07 Almacenado
        */
        $nombres = [
            'Creado',
            'Pasteurizado',
            'Reservado',
            'Vendido',
            'Control de calidad',
            'Baja',
            'Almacenado',
        ];
        $descripciones = [
            'El producto fue creado',
            'El producto pasó por un proceso de pasteurización',
            'Se reservó el producto para una venta',
            'El producto fué vendido',
            'Se realizó un examen de control de calidad aleatorio sobre el producto',
            'El producto fué dado de baja',
            'El producto fue almacenado para mantener stock',
        ];
        for ($i=0 ; $i<7 ; $i++){
            EstadoDeProducto::create([
                'nombre'        =>$nombres[$i],
                'descripcion'   =>$descripciones[$i],
            ]);
        }
    }
}
