<?php

use Illuminate\Database\Seeder;
use App\Models\EstadoDeExtraccion;

class EstadosDeExtracciones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Realizada
            02 Pasteurizada
            03 Fraccionada
            04 Finalizada
            05 Control de calidad
            06 Baja
        */
        $nombres = [
            'Realizada',
            'Pasteurizada',
            'Fraccionada',
            'Finalizada',
            'Control de calidad',
            'Baja',
        ];
        $descripciones = [
            'Extracción realizada',
            'La extracción pasó por un proceso de pasteurización',
            'Se fraccionó parte o la totalidad de la extracción',
            'La extracción se fraccionó completamente',
            'Se realizó un examen de control de calidad aleatorio sobre la extracción',
            'La extracción fué dada de baja',
        ];
        for ($i=0 ; $i<6 ; $i++){
            EstadoDeExtraccion::create([
                'nombre'        =>$nombres[$i],
                'descripcion'   =>$descripciones[$i],
            ]);
        }
    }
}
