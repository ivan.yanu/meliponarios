<?php

use Illuminate\Database\Seeder;
use App\Models\OrdenEstadosDeLote;

class OrdenEstadosDeLotes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Carga de extracción
            03 Completo
            04 Cerrado
            05 Pasteurizado
            06 Pesado
            07 Abierto
            08 Fraccionado
            09 Finalizado
            10 Control de calidad
            11 Baja
        */
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 2,//Carga de extracción
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 1,//Creado
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 2,//Carga de extracción
            'estado_siguiente_id' => 2,//Carga de extracción
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 2,//Carga de extracción
            'estado_siguiente_id' => 3,//Completo
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 2,//Carga de extracción
            'estado_siguiente_id' => 4,//Cerrado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 2,//Carga de extracción
            'estado_siguiente_id' => 6,//Pesado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 2,//Carga de extracción
            'estado_siguiente_id' => 8,//Fraccionado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 2,//Carga de extracción
            'estado_siguiente_id' => 10,//Control de calidad
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 2,//Carga de extracción
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 3,//Completo
            'estado_siguiente_id' => 4,//Cerrado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 3,//Completo
            'estado_siguiente_id' => 6,//Pesado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 3,//Completo
            'estado_siguiente_id' => 8,//Fraccionado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 3,//Completo
            'estado_siguiente_id' => 10,//Control de calidad
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 3,//Completo
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 4,//Cerrado
            'estado_siguiente_id' => 5,//Pasteurizado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 4,//Cerrado
            'estado_siguiente_id' => 6,//Pesado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 4,//Cerrado
            'estado_siguiente_id' => 7,//Abierto
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 4,//Cerrado
            'estado_siguiente_id' => 10,//Control de calidad
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 4,//Cerrado
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 5,//Pasteurizado
            'estado_siguiente_id' => 6,//Pesado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 5,//Pasteurizado
            'estado_siguiente_id' => 7,//Abierto
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 5,//Pasteurizado
            'estado_siguiente_id' => 10,//Control de calidad
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 5,//Pasteurizado
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 6,//Pesado
            'estado_siguiente_id' => 2,//Carga de extracción
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 6,//Pesado
            'estado_siguiente_id' => 4,//Cerrado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 6,//Pesado
            'estado_siguiente_id' => 5,//Pasteurizado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 6,//Pesado
            'estado_siguiente_id' => 7,//Abierto
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 6,//Pesado
            'estado_siguiente_id' => 8,//Fraccionado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 6,//Pesado
            'estado_siguiente_id' => 10,//Control de calidad
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 6,//Pesado
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 7,//Abierto
            'estado_siguiente_id' => 2,//Carga de extracción
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 7,//Abierto
            'estado_siguiente_id' => 4,//Cerrado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 7,//Abierto
            'estado_siguiente_id' => 6,//Pesado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 7,//Abierto
            'estado_siguiente_id' => 8,//Fraccionado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 7,//Abierto
            'estado_siguiente_id' => 10,//Control de calidad
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 7,//Abierto
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 8,//Fraccionado
            'estado_siguiente_id' => 2,//Carga de extracción
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 8,//Fraccionado
            'estado_siguiente_id' => 4,//Cerrado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 8,//Fraccionado
            'estado_siguiente_id' => 6,//Pesado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 8,//Fraccionado
            'estado_siguiente_id' => 8,//Fraccionado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 8,//Fraccionado
            'estado_siguiente_id' => 9,//Finalizado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 8,//Fraccionado
            'estado_siguiente_id' => 10,//Control de calidad
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 8,//Fraccionado
            'estado_siguiente_id' => 11,//Baja
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 10,//Control de calidad
            'estado_siguiente_id' => 2,//Carga de extracción
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 10,//Control de calidad
            'estado_siguiente_id' => 4,//Cerrado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 10,//Control de calidad
            'estado_siguiente_id' => 5,//Pasteurizado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 10,//Control de calidad
            'estado_siguiente_id' => 6,//Pesado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 10,//Control de calidad
            'estado_siguiente_id' => 7,//Abierto
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 10,//Control de calidad
            'estado_siguiente_id' => 8,//Fraccionado
        ]);
        OrdenEstadosDeLote::create([
            'estado_inicial_id'   => 10,//Control de calidad
            'estado_siguiente_id' => 11,//Baja
        ]);
    }
}
