<?php

use Illuminate\Database\Seeder;
use App\Models\OrdenEstadosDeCajon;

class OrdenEstadosDeCajones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Colmena instalada
            03 Colmena mudada
            04 Instalado en meliponario
            05 Trasladado de meliponario
            06 Revisado
            07 Pendiente de reparación
            08 Llevado a reparar
            09 Reparado
            10 Baja
            11 Desarmado para almacenar
        */
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 1, //Creado
            'estado_siguiente_id' => 2, //Colmena instalada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 1, //Creado
            'estado_siguiente_id' => 4, //Instalado en meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 1, //Creado
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 1, //Creado
            'estado_siguiente_id' => 11, //Desarmado para almacenar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 2, //Colmena instalada
            'estado_siguiente_id' => 3, //Colmena mudada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 2, //Colmena instalada
            'estado_siguiente_id' => 4, //Instalado en meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 2, //Colmena instalada
            'estado_siguiente_id' => 5, //Trasladado de meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 2, //Colmena instalada
            'estado_siguiente_id' => 6, //Revisado
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 2, //Colmena instalada
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 3, //Colmena mudada
            'estado_siguiente_id' => 2, //Colmena instalada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 3, //Colmena mudada
            'estado_siguiente_id' => 5, //Trasladado de meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 3, //Colmena mudada
            'estado_siguiente_id' => 8, //Llevado a reparar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 3, //Colmena mudada
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 3, //Colmena mudada
            'estado_siguiente_id' => 11, //Desarmado para almacenar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 4, //Instalado en meliponario
            'estado_siguiente_id' => 2, //Colmena instalada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 4, //Instalado en meliponario
            'estado_siguiente_id' => 5, //Trasladado de meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 4, //Instalado en meliponario
            'estado_siguiente_id' => 6, //Revisado
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 4, //Instalado en meliponario
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 4, //Instalado en meliponario
            'estado_siguiente_id' => 11, //Desarmado para almacenar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 5, //Trasladado de meliponario
            'estado_siguiente_id' => 2, //Colmena instalada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 5, //Trasladado de meliponario
            'estado_siguiente_id' => 3, //Colmena mudada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 5, //Trasladado de meliponario
            'estado_siguiente_id' => 5, //Trasladado de meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 5, //Trasladado de meliponario
            'estado_siguiente_id' => 6, //Revisado
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 5, //Trasladado de meliponario
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 5, //Trasladado de meliponario
            'estado_siguiente_id' => 11, //Desarmado para almacenar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 6, //Revisado
            'estado_siguiente_id' => 2, //Colmena instalada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 6, //Revisado
            'estado_siguiente_id' => 3, //Colmena mudada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 6, //Revisado
            'estado_siguiente_id' => 5, //Trasladado de meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 6, //Revisado
            'estado_siguiente_id' => 6, //Revisado
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 6, //Revisado
            'estado_siguiente_id' => 7, //Pendiente de reparación
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 6, //Revisado
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 6, //Revisado
            'estado_siguiente_id' => 11, //Desarmado para almacenar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 7, //Pendiente de reparación
            'estado_siguiente_id' => 3, //Colmena mudada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 7, //Pendiente de reparación
            'estado_siguiente_id' => 6, //Revisado
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 7, //Pendiente de reparación
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 8, //Llevado a reparar
            'estado_siguiente_id' => 9, //Reparado
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 8, //Llevado a reparar
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 9, //Reparado
            'estado_siguiente_id' => 2, //Colmena instalada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 9, //Reparado
            'estado_siguiente_id' => 4, //Instalado en meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 9, //Reparado
            'estado_siguiente_id' => 10, //Baja
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 9, //Reparado
            'estado_siguiente_id' => 11, //Desarmado para almacenar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 11, //Desarmado para almacenar
            'estado_siguiente_id' => 2, //Colmena instalada
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 11, //Desarmado para almacenar
            'estado_siguiente_id' => 4, //Instalado en meliponario
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 11, //Desarmado para almacenar
            'estado_siguiente_id' => 8, //Llevado a reparar
        ]);
        OrdenEstadosDeCajon::create([
            'estado_inicial_id'   => 11, //Desarmado para almacenar
            'estado_siguiente_id' => 10, //Baja
        ]);
    }
}
