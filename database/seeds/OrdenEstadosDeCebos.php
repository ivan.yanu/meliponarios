<?php

use Illuminate\Database\Seeder;
use App\Models\OrdenEstadosDeCebo;

class OrdenEstadosDeCebos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 Creado
            02 Instalado en meliponario
            03 Mudado de meliponario
            04 Abejas encontradas
            05 Baja
            06 Pasado a cajon
            07 Revisado
        */
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 1, //Creado
            'estado_siguiente_id' => 2, //Instalado en meliponario
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 1, //Creado
            'estado_siguiente_id' => 5, //Baja
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 2, //Instalado en meliponario
            'estado_siguiente_id' => 3, //Mudado de meliponario
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 2, //Instalado en meliponario
            'estado_siguiente_id' => 4, //Abejas encontradas
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 2, //Instalado en meliponario
            'estado_siguiente_id' => 5, //Baja
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 2, //Instalado en meliponario
            'estado_siguiente_id' => 7, //Revisado
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 3, //Mudado de meliponario
            'estado_siguiente_id' => 4, //Abejas encontradas
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 3, //Mudado de meliponario
            'estado_siguiente_id' => 5, //Baja
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 3, //Mudado de meliponario
            'estado_siguiente_id' => 7, //Revisado
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 4, //Abejas encontradas
            'estado_siguiente_id' => 5, //Baja
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 4, //Abejas encontradas
            'estado_siguiente_id' => 6, //Pasado a cajon
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 7, //Revisado
            'estado_siguiente_id' => 3, //Mudado de meliponario
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 7, //Revisado
            'estado_siguiente_id' => 4, //Abejas encontradas
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 7, //Revisado
            'estado_siguiente_id' => 5, //Baja
        ]);
        OrdenEstadosDeCebo::create([
            'estado_inicial_id'   => 7, //Revisado
            'estado_siguiente_id' => 7, //Revisado
        ]);
    }
}
