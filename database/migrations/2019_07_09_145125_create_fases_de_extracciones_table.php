<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasesDeExtraccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fases_de_extracciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha');
            $table->bigInteger('extraccion_id')->unsigned();
            $table->foreign('extraccion_id')->references('id')->on('extracciones');
            $table->bigInteger('estado_de_extraccion_id')->unsigned();
            $table->foreign('estado_de_extraccion_id')->references('id')->on('estados_de_extracciones');
            $table->string('descripcion')->nullable();
            $table->bigInteger('extraccion_por_lote_id')->unsigned()->nullable();
            $table->foreign('extraccion_por_lote_id')->references('id')->on('extracciones_por_lotes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fases_de_extracciones');
    }
}
