<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extracciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('peso_en_gramos')->unsigned()->nullable();
            $table->bigInteger('colmena_id')->unsigned()->nullable();
            $table->foreign('colmena_id')->references('id')->on('colmenas');
            $table->bigInteger('usuario_id')->unsigned()->nullable();
            $table->foreign('usuario_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extracciones');
    }
}
