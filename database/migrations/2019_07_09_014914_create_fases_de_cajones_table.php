s<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasesDeCajonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fases_de_cajones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha');
            $table->bigInteger('cajon_id')->unsigned();
            $table->foreign('cajon_id')->references('id')->on('cajones');
            $table->bigInteger('estado_de_cajon_id')->unsigned();
            $table->foreign('estado_de_cajon_id')->references('id')->on('estados_de_cajones');
            $table->string('descripcion')->nullable();
            $table->bigInteger('colmena_id')->unsigned()->nullable();
            $table->foreign('colmena_id')->references('id')->on('colmenas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fases_de_cajones');
    }
}
