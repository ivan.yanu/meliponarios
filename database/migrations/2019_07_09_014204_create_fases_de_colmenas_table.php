<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasesDeColmenasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fases_de_colmenas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha');
            $table->bigInteger('colmena_id')->unsigned();
            $table->foreign('colmena_id')->references('id')->on('colmenas');
            $table->bigInteger('estado_de_colmena_id')->unsigned();
            $table->foreign('estado_de_colmena_id')->references('id')->on('estados_de_colmenas');
            $table->string('descripcion')->nullable();
            $table->bigInteger('cajon_id')->unsigned()->nullable();
            $table->foreign('cajon_id')->references('id')->on('cajones');
            $table->bigInteger('extraccion_id')->unsigned()->nullble();
            $table->foreign('extraccion_id')->references('id')->on('extracciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fases_de_colmenas');
        
    }
}
