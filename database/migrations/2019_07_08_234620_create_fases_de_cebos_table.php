<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasesDeCebosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fases_de_cebos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha');
            $table->bigInteger('cebo_id')->unsigned();
            $table->foreign('cebo_id')->references('id')->on('cebos');
            $table->bigInteger('estado_de_cebo_id')->unsigned();
            $table->foreign('estado_de_cebo_id')->references('id')->on('estados_de_cebos');
            $table->string('descripcion')->nullable();
            $table->bigInteger('meliponario_id')->unsigned()->nullable();
            $table->foreign('meliponario_id')->references('id')->on('meliponarios');
            $table->bigInteger('colmena_id')->unsigned()->nullable();
            $table->foreign('colmena_id')->references('id')->on('colmenas');
            $table->bigInteger('cajon_id')->unsigned()->nullable();
            $table->foreign('cajon_id')->references('id')->on('cajones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fases_de_cebos');
    }
}
