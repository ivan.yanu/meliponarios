<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraccionesPorLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extracciones_por_lotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('extraccion_id')->unsigned();
            $table->foreign('extraccion_id')->references('id')->on('extracciones');
            $table->bigInteger('lote_id')->unsigned();
            $table->foreign('lote_id')->references('id')->on('lotes');
            $table->integer('peso_en_gramos')->unsigned();
            $table->boolean('pasteurizado');
            $table->dateTime('fecha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extracciones_por_lotes');
    }
}
