<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenEstadosDeCajonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_estados_de_cajones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('estado_inicial_id')->unsigned();
            $table->foreign('estado_inicial_id')->references('id')->on('estados_de_cajones');
            $table->bigInteger('estado_siguiente_id')->unsigned();
            $table->foreign('estado_siguiente_id')->references('id')->on('estados_de_cajones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_estados_de_cajones');
    }
}
