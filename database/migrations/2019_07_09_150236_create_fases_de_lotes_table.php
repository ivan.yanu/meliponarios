<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasesDeLotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fases_de_lotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha');
            $table->bigInteger('lote_id')->unsigned();
            $table->foreign('lote_id')->references('id')->on('lotes');
            $table->bigInteger('estado_de_lote_id')->unsigned();
            $table->foreign('estado_de_lote_id')->references('id')->on('estados_de_lotes');
            $table->string('descripcion')->nullable();
            $table->bigInteger('extraccion_por_lote_id')->unsigned()->nullable();
            $table->foreign('extraccion_por_lote_id')->references('id')->on('extracciones_por_lotes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fases_de_lotes');
    }
}
