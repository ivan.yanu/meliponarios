<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFasesDeMeliponariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fases_de_meliponarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha');
            $table->bigInteger('meliponario_id')->unsigned();
            $table->foreign('meliponario_id')->references('id')->on('meliponarios');
            $table->bigInteger('estado_de_meliponario_id')->unsigned();
            $table->foreign('estado_de_meliponario_id')->references('id')->on('estados_de_meliponarios');
            $table->string('descripcion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fases_de_meliponarios');
    }
}
