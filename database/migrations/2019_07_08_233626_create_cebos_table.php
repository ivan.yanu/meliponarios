<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCebosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cebos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tipo_de_cebo_id')->unsigned()->nullable();
            $table->foreign('tipo_de_cebo_id')->references('id')->on('tipos_de_cebos');
            $table->bigInteger('usuario_id')->unsigned()->nullable();
            $table->foreign('usuario_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cebos');
    }
}
